json.extract! quiz_sessions_step, :id, :created_at, :updated_at
json.url quiz_sessions_step_url(quiz_sessions_step, format: :json)
